import { Component, OnInit } from '@angular/core';
import { ParagraphService } from 'src/app/services/paragraph.service';
import { Paragraph } from 'src/app/classes/paragraph';
import { RowAlphabetCount } from 'src/app/classes/row-alphabet-count';

@Component({
  selector: 'app-paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.css']
})
export class ParagraphComponent implements OnInit {

  loading:boolean = false;
  arrayParagraph:Paragraph[] = [];
  alphabet:string[] = [];
  tableParagraph:RowAlphabetCount[] = [];
  
  constructor(private paragraphService: ParagraphService) { }
  
  ngOnInit() {
    this.generateAlphabet();
  }
  //Generacion de alfabeto
  generateAlphabet(){
    let i = 'a'.charCodeAt(0), j = 'z'.charCodeAt(0);
    for(;i <= j; ++i){
      this.alphabet.push(String.fromCharCode(i));
    }
    return this.alphabet;
  }
  //Llamada a la Api y guardado de respuesta, si no es nula se ordena el array
  getParagraphData(){
    this.loading = true;
    this.paragraphService.getParagraph().subscribe(
      res => {
        this.arrayParagraph = res['data'];
        this.loading = false;
        if(res['data']) this.newRegister(this.arrayParagraph);
      },
      err => {
      }
    );
  }

  //Se añade una nueva fila en la tabla, con los valores obtenidos en el analisis del arreglo de parrafos
  newRegister(array: Paragraph[]){
    let totalSum = this.sumNumbers(array);
    let lettersSum = this.countLetters(array);
    var aux = new RowAlphabetCount(lettersSum, totalSum);
    this.tableParagraph.push(aux);
  }

  //Funcion que mapea los valores numericos de los parrafos y suma su valor absoluto en un total.
  sumNumbers(array: Paragraph[]){
    let numbers = array.map( item => {
      return Math.abs(item['number']);
    } );
    return numbers.reduce((a, b) => a + b, 0);
  }

  //Funcion que contabiliza las letras en los parrafos mediante iteraciones.
  countLetters(array: Paragraph[]){
    let acum, result = [];
    for (const letter of this.alphabet) {
      acum = 0;
      for (const item of array) {
        for (let i = 0; i < item.paragraph.length; i++) {
          if(letter == item.paragraph.charAt(i).toLowerCase()){
            acum += 1;
          }
        }
      }
      result.push(acum);
    }
    return result;
  }

}
