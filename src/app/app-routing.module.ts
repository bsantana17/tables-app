import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//components
import {ArrayComponent} from './components/array/array.component';
import {ParagraphComponent} from './components/paragraph/paragraph.component';

const routes: Routes = [
    {path: 'array', component: ArrayComponent},
    {path: 'paragraph', component: ParagraphComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'array'}
  ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { } 