//Clase de los datos parrafos
export class Paragraph {
    hasCopyright: boolean;
    number: number;
    paragraph: string;
    constructor(hasCopyright: boolean, number: number, paragraph: string){
        this.hasCopyright = hasCopyright;
        this.number = number;
        this.paragraph = paragraph;
    }
}
