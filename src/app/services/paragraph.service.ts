import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParagraphService {

  private paragraphUrl = environment.api + "dict";

  constructor(private http: HttpClient) { }

  public getParagraph(){
    return this.http.get(this.paragraphUrl);
  }

}
