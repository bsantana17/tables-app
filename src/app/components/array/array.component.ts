import { Component, OnInit } from '@angular/core';
import { ArrayService } from 'src/app/services/array.service';
import { Number } from '../../classes/Number';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})
export class ArrayComponent implements OnInit {

  arrayNumber:number[] = []; //Array obtenido del servidor
  loading:boolean = false; //Bandera para mostrar Spinner
  tableNumber:Number[] = []; //Arreglo con cada registro obtenido del servidor

  constructor(private arrayService: ArrayService) { }

  ngOnInit() {

  }

  //Llamada a la Api y guardado de respuesta, si no es nula se ordena el array
  getNumberData(){
    this.loading = true;
    this.arrayService.getArray().subscribe(
        res => {
          this.arrayNumber = res['data'];
          this.loading = false;
          if(res['data']) this.newRegister(this.arrayNumber);
        },
        err => {
        }
      );
  }

  //Se añade una nueva fila en la tabla, la columna number se interpreta
  //como el numero de la solicitud. Se llama al constructor de la clase Number
  //para crear un registro, finalmente se ordena el array.
  newRegister(array: number[]){
    let num = this.tableNumber.length + 1;
    let quantity = this.countDuplicate(num, array);
    let aux = new Number(num, quantity, array[0], array[array.length -1]);
    this.tableNumber.push(aux);
    this.sortAscending();
  }

  //Metodo burbuja para ordenar el array
  sortAscending(){
    let done = false;
    while (!done) {
      done = true;
      for (let i = 1; i < this.arrayNumber.length; i += 1) {
        if (this.arrayNumber[i - 1] > this.arrayNumber[i]) {
          done = false;
          let tmp = this.arrayNumber[i - 1];
          this.arrayNumber[i - 1] = this.arrayNumber[i];
          this.arrayNumber[i] = tmp;
        }
      }
    }
  }

  //Iteracion para contar los duplicados del 'number'
  countDuplicate(num: number, array: number[]){
    let acum = 0;
    for (let i = 0; i < array.length; i++) {
      if (array[i] === num) {
        acum++;
      }
    }
    return acum;
  }

}
