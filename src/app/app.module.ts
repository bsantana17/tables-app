import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ArrayComponent } from './components/array/array.component';
import { ParagraphComponent } from './components/paragraph/paragraph.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ArrayComponent,
    ParagraphComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
