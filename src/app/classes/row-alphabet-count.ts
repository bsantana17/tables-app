//Clase para cada registro en la tabla de parrafos
export class RowAlphabetCount {
    values: number[];
    sum: number;
    constructor(values: number[], sum: number){
        this.values = values;
        this.sum = sum;
    }
}
