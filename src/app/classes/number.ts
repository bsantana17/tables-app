//Clase para la tabla de arreglos de numeros
export class Number {
    number: number;
    quantity: number;
    first: number;
    last: number;

    constructor(number:number, quantity:number, first:number, last:number){
        this.number = number;
        this.quantity = quantity;
        this.first = first;
        this.last = last;
    }
}
